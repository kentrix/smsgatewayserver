use std::time::{Duration, Instant};

use actix::*;
use actix_web::{web, App, HttpResponse, HttpRequest, HttpServer, Responder, Error};
use actix_files as fs;
use actix_web_actors::ws;
use crate::server::{SMSMessage, WithoutAPIKey};
use std::fs as sfs;
use toml;
use serde::{Deserialize, Serialize};
use std::clone::Clone;

mod server;

/// How often heartbeat pings are sent
const HEARTBEAT_INTERVAL: Duration = Duration::from_secs(5);
/// How long before lack of client response causes a timeout
const CLIENT_TIMEOUT: Duration = Duration::from_secs(10);


/// Entry point for our route
async fn ws_route(
    req: HttpRequest,
    stream: web::Payload,
    data: web::Data<AppState>,
) -> Result<HttpResponse, Error> {
    ws::start(
        WsSession {
            id: 0,
            hb: Instant::now(),
            name: None,
            addr: data.server.clone(),
        },
        &req,
        stream,
    )
}

struct WsSession {
    /// unique session id
    id: usize,
    /// Client must send ping at least once per 10 seconds (CLIENT_TIMEOUT),
    /// otherwise we drop connection.
    hb: Instant,
    /// peer name
    name: Option<String>,
    /// Chat server
    addr: Addr<server::WSServer>,
}

impl WsSession {
    /// helper method that sends ping to client every second.
    ///
    /// also this method checks heartbeats from client
    fn hb(&self, ctx: &mut ws::WebsocketContext<Self>) {
        ctx.run_interval(HEARTBEAT_INTERVAL, |act, ctx| {
            // check client heartbeats
            if Instant::now().duration_since(act.hb) > CLIENT_TIMEOUT {
                // heartbeat timed out
                println!("Websocket Client heartbeat failed, disconnecting!");

                // notify chat server
                act.addr.do_send(server::Disconnect { id: act.id });

                // stop actor
                ctx.stop();

                // don't try to send a ping
                return;
            }

            ctx.ping(b"");
        });
    }
}

impl Actor for WsSession {
    type Context = ws::WebsocketContext<Self>;

    /// Method is called on actor start.
    /// We register ws session with ChatServer
    fn started(&mut self, ctx: &mut Self::Context) {
        // we'll start heartbeat process on session start.
        self.hb(ctx);

        // register self in chat server. `AsyncContext::wait` register
        // future within context, but context waits until this future resolves
        // before processing any other events.
        // HttpContext::state() is instance of WsChatSessionState, state is shared
        // across all routes within application
        let addr = ctx.address();
        self.addr
            .send(server::Connect {
                addr: addr.recipient(),
            })
            .into_actor(self)
            .then(|res, act, ctx| {
                match res {
                    Ok(res) => act.id = res,
                    // something is wrong with ws server
                    _ => ctx.stop(),
                }
                fut::ready(())
            })
            .wait(ctx);
    }

    fn stopping(&mut self, _: &mut Self::Context) -> Running {
        // notify chat server
        self.addr.do_send(server::Disconnect { id: self.id });
        Running::Stop
    }
}

impl Handler<server::Message> for WsSession {
    type Result = ();

    fn handle(&mut self, msg: server::Message, ctx: &mut Self::Context) {
        ctx.text(msg.0);
    }
}

impl Handler<server::SendSMSMessage> for WsSession {
    type Result = ();

    fn handle(&mut self, msg: server::SendSMSMessage, ctx: &mut Self::Context) {
        ctx.text(serde_json::to_string(&msg.message).unwrap());
    }
}

/// WebSocket message handler
impl StreamHandler<Result<ws::Message, ws::ProtocolError>> for WsSession {
    fn handle(
        &mut self,
        msg: Result<ws::Message, ws::ProtocolError>,
        ctx: &mut Self::Context,
    ) {
        let msg = match msg {
            Err(_) => {
                ctx.stop();
                return;
            }
            Ok(msg) => msg,
        };

        println!("WEBSOCKET MESSAGE: {:?}", msg);
        match msg {
            ws::Message::Ping(msg) => {
                self.hb = Instant::now();
                ctx.pong(&msg);
            }
            ws::Message::Pong(_) => {
                self.hb = Instant::now();
            }
            ws::Message::Text(text) => {
                let m = text.trim();
                return ();
            }
            ws::Message::Binary(_) => println!("Unexpected binary"),
            ws::Message::Close(_) => {
                ctx.stop();
            }
            ws::Message::Continuation(_) => {
                ctx.stop();
            }
            ws::Message::Nop => (),
        }
    }
}

#[derive(Deserialize, Serialize, Clone)]
pub struct AppConfig {
    pub api_key: String
}

#[derive(Clone)]
pub struct AppState {
    pub server: Addr<server::WSServer>,
    pub config: AppConfig,
}

#[actix_rt::main]
async fn main() -> std::io::Result<()> {
    let config_str = sfs::read_to_string("Config.toml").unwrap();
    let config: AppConfig = toml::from_str(&config_str).unwrap();

    env_logger::init();

    // Start chat server actor
    let server = server::WSServer::default().start();
    let state = AppState {
        config,
        server: server.clone(),
    };

    // Create Http server with websocket support
    HttpServer::new(move || {
        App::new()
            .data(state.clone())
            // redirect to websocket.html
            .service(web::resource("/").route(web::get().to(|| {
                HttpResponse::Found()
                    .header("LOCATION", "/static/websocket.html")
                    .finish()
            })))
            // websocket
            .service(web::resource("/ws/").to(ws_route))
            // static resources
            .service(fs::Files::new("/static/", "static/"))
            .route("/sms", web::post().to(receiveMsg))
    })
        .bind("0.0.0.0:8080")?
        .start()
        .await
}

async fn receiveMsg(mut msg: web::Json<server::SMSMessageWithAPIKey>, data: web::Data<AppState>) -> impl Responder {
    if msg.apiKey == data.config.api_key {
        data.server.do_send(server::SendSMSMessage {
            message: msg.getWithOutAPIKey()
        });
        HttpResponse::NoContent()
    } else {
        HttpResponse::Unauthorized()
    }
}
