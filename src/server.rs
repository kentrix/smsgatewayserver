use serde::{Deserialize, Serialize};
use serde_json::Result;
use actix::prelude::*;
use rand::{self, rngs::ThreadRng, Rng};
use std::collections::{HashMap, HashSet};

/// Chat server sends this messages to session
#[derive(Message)]
#[rtype(result = "()")]
pub struct Message(pub String);

/// Message for chat server communications

/// New chat session is created
#[derive(Message)]
#[rtype(usize)]
pub struct Connect {
    pub addr: Recipient<Message>,
}

/// Session is disconnected
#[derive(Message)]
#[rtype(result = "()")]
pub struct Disconnect {
    pub id: usize,
}

pub trait WithoutAPIKey {
    type S;
    fn getWithOutAPIKey(&mut self) -> Self::S;
}

#[derive(Serialize, Deserialize)]
pub struct SMSMessage {
    pub from: String,
    pub body: String,
    pub receivedAt: String,
    pub phoneNumber: String,
}

#[derive(Serialize, Deserialize)]
pub struct SMSMessageWithAPIKey {
    pub from: String,
    pub body: String,
    pub receivedAt: String,
    pub phoneNumber: String,
    pub apiKey: String
}

impl WithoutAPIKey for SMSMessageWithAPIKey {
    type S = SMSMessage;

    fn getWithOutAPIKey(&mut self) -> Self::S {
        return SMSMessage {
            from: self.from.to_owned(),
            body: self.body.to_owned(),
            receivedAt: self.receivedAt.to_owned(),
            phoneNumber: self.phoneNumber.to_owned(),
        }
    }
}

#[derive(Message)]
#[rtype(result = "()")]
pub struct SendSMSMessage {
    pub message: SMSMessage
}

pub struct WSServer {
    sessions: HashMap<usize, Recipient<Message>>,
    rng: ThreadRng,
}

impl WSServer {
    fn send_message(&self, message: &str) {
        for ref session in self.sessions.values() {
            session.do_send(Message(message.to_owned()));
        }
    }
}

impl Default for WSServer {
    fn default() -> WSServer {
        WSServer {
            sessions: HashMap::new(),
            rng: rand::thread_rng(),
        }
    }
}

/// Make actor from `ChatServer`
impl Actor for WSServer {
    /// We are going to use simple Context, we just need ability to communicate
    /// with other actors.
    type Context = Context<Self>;
}

/// Handler for Connect message.
///
/// Register new session and assign unique id to this session
impl Handler<Connect> for WSServer {
    type Result = usize;

    fn handle(&mut self, msg: Connect, _: &mut Context<Self>) -> Self::Result {
        println!("Someone connected");

        // register session with random id
        let id = self.rng.gen::<usize>();
        self.sessions.insert(id, msg.addr);
        // send id back
        id
    }
}

/// Handler for Disconnect message.
impl Handler<Disconnect> for WSServer {
    type Result = ();

    fn handle(&mut self, msg: Disconnect, _: &mut Context<Self>) {
        println!("Someone disconnected");

        self.sessions.remove(&msg.id);
    }
}

impl Handler<SendSMSMessage> for WSServer {
    type Result = ();

    fn handle<'a>(&mut self, msg: SendSMSMessage, ctx: &mut Context<Self>) -> Self::Result {
       for session in self.sessions.values() {
           session.do_send(Message(serde_json::to_string(&msg.message).unwrap()));
       }
    }
}

