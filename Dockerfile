FROM rust as cargo-build

RUN mkdir /app

COPY ./Cargo.toml /app/

COPY ./Cargo.lock /app/

COPY ./src /app/src

WORKDIR /app

RUN cargo build --release

FROM ubuntu

RUN mkdir /app

COPY ./static /app/static
COPY ./Config.toml /app/

COPY --from=cargo-build /app/target/release/SMSGateway /app

EXPOSE 8080

WORKDIR /app

CMD ["./SMSGateway"]


